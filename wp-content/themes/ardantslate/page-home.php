<?php /* Template name: Home */ get_header(); ?>
<section id="content" role="main" class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="event-header col-xs-12 col-md-6">
	<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
    <h2 class="event-title"><?php the_field('event_title');?></h2>
    <?php the_field('venue_text');?>
</header>
<section class="home-production col-xs-12 col-md-6">
	<?php if ( has_post_thumbnail() ) { the_post_thumbnail('large'); } ?>
</section>
<section class="entry-content col-xs-12">

<?php the_content(); ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>