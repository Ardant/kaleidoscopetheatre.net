<?php /*template name: Contact*/ get_header(); ?>
<section id="content" role="main" class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content col-xs-12 col-sm-6">
<figure class="post-thumbnail"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></figure>
<?php the_content(); ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
<section class="contact-form col-xs-12 col-sm-6">
	<?php echo do_shortcode( '[contact-form-7 id="48" title="Contact form 1"]' ); ?>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>