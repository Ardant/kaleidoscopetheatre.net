<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
<p>&copy; <?php echo date("Y") ?> Kaleidoscope Theatre &nbsp;|&nbsp; Charity Number: 517839 &nbsp;|&nbsp; Tel:&nbsp;01952 588766 &nbsp;|&nbsp; Website by <a href="http://ardant.co.uk">Ardant Design Ltd</a></p>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>