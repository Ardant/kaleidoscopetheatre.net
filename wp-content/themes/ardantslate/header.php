<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Dancing+Script|Raleway:400,400i,600,600i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/bootslate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>?<?php echo rand(1,999999999999999); ?>" />
<link type="text/plain" rel="author" href="/humans.txt" />
<?php wp_head(); ?>
</head>
<body ontouchstart="" <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
    <section id="branding">
    	<figure id="jesters">
        	<img src="<?php echo get_template_directory_uri()?>/img/jesters_min.png" alt="Kaleidoscope Theatre Company" />
        </figure>
        <div id="site-title">
			<?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } else { echo '<span>';} ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><?php bloginfo( 'description' ); ?></a><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } else { echo '</span>';} ?>
		</div>
    </section>
    <nav id="menu" role="navigation">
    <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
    </nav>
</header>
<div id="main" class="container">