<?php /* Template name: Reviews */ get_header(); ?>
<section id="content" role="main" class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<div class="entry-content col-xs-12">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
<?php the_content(); ?>
</div>
<section class="col-xs-12">
<?php 
	$letters = array();
    $args = array('post_type' => 'review',
		'order'=>'ASC',	
     );
     $loop = new WP_Query($args);
     while($loop->have_posts()) : $loop->the_post();?>
		<section class="production">
	        <h1><?php the_title(); ?></h1>
	        <figure>
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail('large'); } ?>
			</figure>
            <?php the_content();?>
        </section>
     <?php endwhile;?>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>