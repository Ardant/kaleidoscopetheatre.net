<?php
/*
Plugin Name: Ardant WP
Description: Ardant Wordpress Plugin.
Author: Daniel Smith
Author URI: https://ardant.co.uk
Version: 1.0.0
Plugin URI: https://ardant.co.uk/
Copyright: Daniel Smith
Text Domain: ardant-wp
*/

add_action( 'init', 'cpt_review' );
function cpt_review() {
	register_post_type( 'review', array(
	  'labels' => array(
	    'name' => 'Reviews',
	    'singular_name' => 'Review',
	   ),
	  'description' => 'Custom post type for reviews',
	  'public' => true,
	  'menu_position' => 10,
	  'supports' => array( 'title','editor','thumbnail')
	));
}













?>